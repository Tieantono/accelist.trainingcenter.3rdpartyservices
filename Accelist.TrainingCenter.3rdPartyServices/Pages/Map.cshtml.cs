﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace Accelist.TrainingCenter._3rdPartyServices.Pages
{
    public class MapModel : PageModel
    {
        private readonly IConfiguration Configuration;

        public MapModel(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public string GoogleMapsApiKey { get; set; }

        public void OnGet()
        {
            // Read appsettings value from "Google" -> "Maps" -> "ApiKey".
            this.GoogleMapsApiKey = this.Configuration["Google:Maps:ApiKey"];
        }
    }
}