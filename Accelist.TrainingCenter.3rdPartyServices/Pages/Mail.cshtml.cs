﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Accelist.TrainingCenter._3rdPartyServices.Pages
{
    public class SendMailModel
    {
        /// <summary>
        /// The requested email address.
        /// </summary>
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// The requested email alias name.
        /// </summary>
        [Required]
        [Display(Name = "Name")]
        public string EmailName { get; set; }
    }

    public class MailModel : PageModel
    {
        public void OnGet()
        {

        }

        /// <summary>
        /// On post property for storing the submitted form data.
        /// </summary>
        [BindProperty]
        public SendMailModel MailData { get; set; }

        public async Task OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                // Give error message to the user.
            }

            // SendGrid API reference: https://sendgrid.com/docs/api-reference/.
            // C# library API reference: https://github.com/sendgrid/sendgrid-csharp.
            var apiKey = Environment.GetEnvironmentVariable(Constants.EnvironmentVariables.SendGridApiKey);

            var client = new SendGridClient(apiKey);
            var fromEmail = Environment.GetEnvironmentVariable(Constants.EnvironmentVariables.SendGridFromEmail);
            var fromName = Environment.GetEnvironmentVariable(Constants.EnvironmentVariables.SendGridFromName);
            var from = new EmailAddress(fromEmail, fromName);
            var subject = "Accelist Trainers Welcome You!";

            // You're free to assign the email destination.
            var to = new EmailAddress(this.MailData.EmailAddress, this.MailData.EmailName);

            var plainTextContent = "Hello guys!";
            var htmlContent = "<strong>Hello guys!</strong>";

            var message = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);

            var response = await client.SendEmailAsync(message);

            // If success, response's status code should be 202.
            if (response.StatusCode != System.Net.HttpStatusCode.Accepted)
            {
                // Give error message to the user.
            }
        }
    }
}