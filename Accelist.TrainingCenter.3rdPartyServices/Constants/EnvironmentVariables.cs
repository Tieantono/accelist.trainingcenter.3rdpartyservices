﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter._3rdPartyServices.Constants
{
    public class EnvironmentVariables
    {
        public const string SendGridApiKey = "SENDGRID_API_KEY";

        public const string SendGridFromEmail = "SENDGRID_FROM_EMAIL";

        public const string SendGridFromName = "SENDGRID_FROM_NAME";
    }
}
